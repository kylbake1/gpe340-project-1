﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerData : MonoBehaviour
{
    // This section is public and customizable in the inspector for the designers
    public float moveSpeed; // in meters per seconds
    public float reverseSpeed;  // this will move the player backwards
    public float turnSpeed = 180f; // in degrees per second
    public float fireRate; // This will control how fast the player can shot per second
    public float bulletSpeed; // This how fast the bullet will travel
    public float bulletDestroyTime; // This is how long the bullet will last before being destroyed
    public float waitTillNextFire; // This will work with firerate to control shooting


    // This section is private and is not customizable to the designers
    // - There is no private variables in this section -

}
