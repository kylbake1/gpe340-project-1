﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMotor : MonoBehaviour
{
    // This section is public and customizable in the inspector for the designers
    public Transform tf; // this will hold out transform componenet and it is viewable in the inspector

    // This section is private and is not customizable to the designers
    private CharacterController characterController; // this will hold our CharacterContoller and it is private so it wont appear in the inspector or other scripts can use it

    // Start is called before the first frame update    
    void Start()
    {
        characterController = gameObject.GetComponent<CharacterController>(); // this will load our Character Controller and gives us access to the CharacterController component 
        tf = gameObject.GetComponent<Transform>();
    }

    public void Move(float Speed) // This fuction is public and will move the AI and Player tanks
    {
        Vector3 speedVector; // This vector3 will hold out speed
        speedVector = tf.forward; // This will make sure our vector is facing the same direction as our tank
        speedVector *= Speed; // This will allow us to control the speed of our tanks
        characterController.SimpleMove(speedVector); // call SimpleMove() and send it our vector3 speed
    }

    public void Rotate(float speed) // This function is public and will rotate the tanks for us with the float speed for adjustments by the designer
    {
        Vector3 rotateVector; // This vector3 will hold out speed
        rotateVector = Vector3.up;//This will ensure that we are rotating on the right axis, the y axis
        rotateVector *= speed; // This can be adjusted by the designer in TankData
        rotateVector *= Time.deltaTime; // This will rotate the tank per second by using time.deltaTime
        tf.Rotate(rotateVector, Space.Self); // This will rotate our tank by using the speed by the user and rotate around the tank and not the world
    }
}
