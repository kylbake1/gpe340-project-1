﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputController : MonoBehaviour
{
    //This section of variable is open to the designer and can be edited
    public enum InputScheme { WASD }; // this will create a drop down box so we can choose which method to use
    public InputScheme input = InputScheme.WASD; // this will set our input to WASD as default
    public PlayerMotor motor; // This will hold out playerMotor script and call it motor
    public PlayerData data; // This will hold out playerData script and call it data
    public Rigidbody bulletPrefab; // this will create a slot in the inspector for our bulletprefab
    public Transform fireTransform; // this will create a slot for our firetransform in the inspector

    //This section of variable is closed to the designer and cannot be edited
    Animator anim; // this will hold the Animator and call it anim
    private Camera mainCamera; // this will hold our Camera and call it mainCamera;

    // Start is called before the first frame update
    void Start()
    {
        if (data == null) // This will continue the game if nothing is set in the following below
        {
            data = gameObject.GetComponent<PlayerData>(); // PlayerData is automatically set for us
            motor = gameObject.GetComponent<PlayerMotor>(); // PlayerMotor is automaticallt set for us
            mainCamera = FindObjectOfType<Camera>(); // Camera is automatically set for us
            anim = GetComponent<Animator>(); // Animator is automatically set for us
        }
    }

    // Update is called once per frame
    void Update()
    {
        switch (input) // this switch will allow us to change inputschemes
        {
            case InputScheme.WASD: // this scheme is called WASD
                if (Input.GetKey(KeyCode.W)) // this will move the player forward
                {
                    motor.Move(data.moveSpeed); // the movement is set to the designers choosing
                    anim.SetInteger("Condition", 1); // this will play the animation and set the integer to 1
                }
                else
                {
                    anim.SetInteger("Condition", 0); // this will set the integer back to 0 when it's not in use
                }
                if (Input.GetKey(KeyCode.S)) // this will move the player backwards/reverse
                {
                    motor.Move(-data.reverseSpeed);// the reverse movement is set to the designers choosing
                }
                if (Input.GetKey(KeyCode.D)) // this will turn the player right
                {
                    motor.Rotate(data.turnSpeed);// the turning is set to the designers choosing
                }
                if (Input.GetKey(KeyCode.A)) // this will turn the player left
                {
                    motor.Rotate(-data.turnSpeed);// the turning is set to the designers choosing
                }
                break; // this will end the function so it wont continue to the next case
        }
        LookTowards(); // calls the LookTowards function and saves in processing 
    }


    void LookTowards()
    {
        Ray cameraRay = mainCamera.ScreenPointToRay(Input.mousePosition); // this creates a Ray and calls it cameraRay for the main camera and will point in the direction that the mouse moves
        Plane groundPlane = new Plane(Vector3.up, Vector3.zero); // this will stop the array from moving beyond our plane
        float rayLength; // this will show how long the ray is

        if (groundPlane.Raycast(cameraRay, out rayLength))
        {
            Vector3 pointToLook = cameraRay.GetPoint(rayLength); // this will give us the postion that the ray is pointing at for the player
            Debug.DrawLine(cameraRay.origin, pointToLook, Color.blue); // this will display the array in a blue color for us to see

            transform.LookAt(new Vector3(pointToLook.x, transform.position.y, pointToLook.z)); // this will rotate the player in the direction of the vector point
        }
    }
}