﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    //This section of variable is open to the designer and can be edited
    public GameObject player; // this allows us to attach our player into the camera

    //This section of variable is closed to the designer and cannot be edited
    private Vector3 offset; // This will allow how far the camera is from the player

    // Start is called before the first frame update
    void Start()
    {
        offset = transform.position - player.transform.position; // this set offset equal to the cameras postions and subtracts the players postion
    }

    // Update is called once per frame
    private void LateUpdate()
    {
        transform.position = player.transform.position + offset; // this will change the cameras postion based on the playes changing postion with the offset given
    }
}
